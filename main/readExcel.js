var fs = require('fs');
const xlsxFile = require('read-excel-file/node');

xlsxFile('./DATABASE.xlsx', { sheet: 'PERSONAS' }).then((rows) => {
    const header = rows[0];
    var data=[];
    for (let index = 1; index < rows.length; index++) {
        var item={};
        const element = rows[index];
        for (cel in element) {
            if(element[cel]!=null){
                item[header[cel]]=element[cel];
            }
        }
        data.push(item);
    }
    fs.writeFile('./data/personas.json', JSON.stringify({"personas":data}), 'utf8',callback);
})

function callback(){
 console.log("operation end");
}